import palmetto.*;

public class Solution {

    public static void main(String[] args) {
        OrderClass order1 = new OrderClass(7717);
        order1.addPizza("Margarita", "Calzone", 2);
        order1.addPizza("Pepperoni", "Regular", 3);
        order1.addIngredients("Pepperoni", "Salami");
        order1.addIngredients("Pepperoni", "Cheese");
        order1.addIngredients("Pepperoni", "Garlic");
        order1.showOrder();
        order1.showOrderTime();
//        order1.printCheck();
        OrderClass order2 = new OrderClass(4372);
        order2.addPizza("Base ZZ", "Regular", 12);
    }
}
