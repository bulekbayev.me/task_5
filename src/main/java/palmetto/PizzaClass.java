package palmetto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

class PizzaClass {

    private String name;
    private String type;
    private ArrayList<String> ingredients;
    private int quantity;

    protected PizzaClass(String name, String type, int quantity) {
        this.name = name;
        this.type = type;
        this.quantity = quantity;
        this.ingredients = new ArrayList<String>();
    }

    //Добавить ингредиент в пиццу
    protected void addIngredient(String ingredient) {
        if (this.ingredients.size() < 7) {
            if (!this.ingredients.contains(ingredient)) {
                this.ingredients.add(ingredient);
                this.ingredients.trimToSize();
            } else {
                System.out.println("You're already have " + ingredient + " in your " + getName() + " pizza. Please check your order again.");
            }
        } else {
            System.out.println("You've reached maximum count of ingredients. You can't add " + ingredient);
        }
    }

    protected String getName() {
        return name;
    }

    protected String getType() {
        return type;
    }

    protected int getQuantity() {
        return quantity;
    }

    protected void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    //Печать ингредиентов пиццы для чека
    protected void printIngredients() {
        for (int i = 0; i < this.ingredients.size(); i++) {
            System.out.println(this.ingredients.get(i) + " : " +  String.format("%.1f",priceTable(this.ingredients.get(i))) + " €");
        }
    }

    //Печать раздела чека с информацией о пиццах
    protected void printAttributes() {
        System.out.println("--------------------------------" + "\n" +
                           "Название: " + getName() + '\n' +
                           "--------------------------------" + "\n" +
                           "Pizza Base (" + getType() + ")" + " : " + String.format("%.1f", priceTable(getType())) + " €");
        printIngredients();
        System.out.println("--------------------------------" + "\n" +
                           "Всего: " + String.format("%.1f", price()) + " €" + "\n" +
                           "Кол-во: " + getQuantity());
    }

    //Цены на игредиенты
    protected double priceTable(String ingredient) {
        Map<String, Double> list = new HashMap<String, Double>();
        list.put("Calzone", 1.5);
        list.put("Regular", 1.0);
        list.put("Tomato Paste", 1.0);
        list.put("Cheese", 1.0);
        list.put("Salami", 1.5);
        list.put("Bacon", 1.2);
        list.put("Garlic", 0.3);
        list.put("Corn", 0.7);
        list.put("Pepperoni", 0.6);
        list.put("Olives", 0.5);
        if (list.containsKey(ingredient)) {
            return list.get(ingredient);
        } else {
            return 0.0;
        }
    }

    //Подсчет общей суммы
    protected double totalPrice() {
        double sum = priceTable(getType());
        for (int i = 0; i < this.ingredients.size(); i++) {
            sum += priceTable(this.ingredients.get(i));
        }
        return sum * getQuantity();
    }

    //Подсчет суммы пиццы
    protected double price() {
        double sum = priceTable(getType());
        for (int i = 0; i < this.ingredients.size(); i++) {
            sum += priceTable(this.ingredients.get(i));
        }
        return sum;
    }
}
