package palmetto;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class OrderClass {

    private int orderID;
    private int clientID;
    private ArrayList<PizzaClass> pizzaList;
    private final int MAX_COUNT = 10;
    private Date dateOfOrder;

    public OrderClass(int clientID) {
        this.orderID = (int) ( Math.random() * (99999 - 10000) ) + 10000;
        this.clientID = clientID;
        pizzaList = new ArrayList<PizzaClass>();
        this.dateOfOrder = new Date();
    }

    //Добавить пиццу
    public void addPizza(String name, String type, int quantity) {
        if (quantity <= MAX_COUNT) {
            if (name.length() > 3 && name.length() < 21) {
                this.pizzaList.add(new PizzaClass(name, type, quantity));
                this.pizzaList.trimToSize();
            } else {
                this.pizzaList.add(new PizzaClass("client_name_" + clientID, type, quantity));
                this.pizzaList.trimToSize();
            }
        } else {
            System.out.println("Maximum quantity of one type of pizza is " + MAX_COUNT + ". Please check your order.");
        }
    }

    //Добавить ингредиент в пиццу из списка pizzaList
    public void addIngredients(String name, String ingredients) {
        for (int i = 0; i < this.pizzaList.size(); i++) {
            if (this.pizzaList.get(i).getName().equals(name)) {
                this.pizzaList.get(i).addIngredient(ingredients);
            }
        }
    }

    //Вывот аттрибутов пицц в заказе
    public void showOrder() {
        for (int i = 0; i < this.pizzaList.size(); i++) {
            System.out.println("[" + orderID + " : " + clientID + " : " + pizzaList.get(i).getName() + " : " + pizzaList.get(i).getQuantity() + "]");
        }
    }

    //Изменить количество пицц в заказе (по имени)
    public void changeOrder(String name, int quantity) {
        for (int i = 0; i < this.pizzaList.size(); i++) {
            if (this.pizzaList.get(i).getName().equals(name)) {
                this.pizzaList.get(i).setQuantity(quantity);
            } else {
                System.out.println("There is no such pizza in your order " + name);
            }
        }
    }

    //Распечатать чек (вариант метода toString из описания задания)
    public void printCheck() {
        double total = 0.0;
        System.out.println("********************************" + "\n" +
                           "Заказ: " + orderID + "\n" +
                           "Клиент: " + clientID);
        for (int i = 0; i < this.pizzaList.size(); i++) {
            this.pizzaList.get(i).printAttributes();
            total += this.pizzaList.get(i).totalPrice();
        }
        System.out.println("--------------------------------" + "\n" +
                           "Общая сумма: " + String.format("%.1f", total) + " €" + "\n" +
                           "********************************");
    }

    //Вывод времени заказа
    public void showOrderTime() {
        System.out.println("Date and time of order #" + orderID + ": " + new SimpleDateFormat("k:mm:ss dd.MM.yyyy").format(this.dateOfOrder));
    }
}
